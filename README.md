[![pipeline status](https://gitlab.com/adak.io/Tools/docker.registry/badges/master/pipeline.svg)](https://gitlab.com/adak.io/Tools/docker.registry/commits/master)
[![coverage report](https://gitlab.com/adak.io/Tools/docker.registry/badges/master/coverage.svg)](https://gitlab.com/adak.io/Tools/docker.registry/commits/master)
## Hello World

#### what is it ?
this is a docker hub mirror on gitlab registry. some people like us doesnt have directly access to docker main registry (maybe country Sanctions or censorship).
you can easily use this as your docker mirror alternative 

#### is it safe ?
yes of course. how ? 
1. you can browse our source code 
2. you can compare our docker tag id & docker image finger print with hub.docker.com images. They should be like that  

#### How can I use this?
1. browse registry page ( https://gitlab.com/adak.io/Tools/docker.registry/container_registry )
2. click on image name
3. chose a version
4. click on clipboard icon <i aria-hidden="true" class="fa fa-clipboard"></i>
5. paste on your terminal/cmd or use in Dockerfile 

#### how does this work?
this is based on gitlab ci service. you can take a look at .gitlab-ci.yml file in the repository.
every week a scheduler start a job in latest master branch. its check docker hub for new images update and if new update is available update our registry (read puller.sh file) 

#### And you can take a look :
**registry** > https://gitlab.com/adak.io/Tools/docker.registry/container_registry  
**wiki** > https://gitlab.com/adak.io/Tools/docker.registry/wikis/pages  
**issues** > https://gitlab.com/adak.io/Tools/docker.registry/issues  
**pipelines** > https://gitlab.com/adak.io/Tools/docker.registry/pipelines  
**project_members** > https://gitlab.com/adak.io/Tools/docker.registry/project_members  