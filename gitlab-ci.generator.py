#!/usr/bin/env python3

import os
import datetime
import yaml

from docker_images_list import docker_images_list

data = {
    'image': 'docker:latest',
    'stages': ['test', 'deploy'],
    'services': ['docker:dind'],
    'cache': {'key': '$CI_JOB_STAGE', 'untracked': True},
    'variables': {'DOCKER_DRIVER': 'overlay2',
                  'GIT_STRATEGY': 'fetch',
                  'GIT_SUBMODULE_STRATEGY': 'none'},
    'before_script': [
        'docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY',
        # 'docker login -u "$DOCKER_REGISTRY_USER" -p "$DOCKER_REGISTRY_PASSWORD"'
    ]
}

for image in docker_images_list:
    for tag in image[1]:
        image_name = '{}:{}'.format(image[0], tag)

        if image_name.startswith('store/'):
            clean_image_name = image_name.replace('store/', '')
        else:
            clean_image_name = image_name

        new_image_data = {
            'copy {} image'.format(image_name): {
                'when': 'always',
                'retry': 2,
                'stage': 'deploy',
                'allow_failure': True,
                'script': [
                    'docker pull {}'.format(image_name),
                    'docker tag {image} "$CI_REGISTRY_IMAGE"/{clean_image}'.format(image=image_name,
                                                                                   clean_image=clean_image_name),
                    'docker push "$CI_REGISTRY_IMAGE"/{}'.format(clean_image_name)
                ],
                'only': ['master'],
            }
        }
        data.update(new_image_data)

if os.path.exists('.gitlab-ci.yml'):
    os.rename('.gitlab-ci.yml', '.gitlab-ci_{now}.yml'.format(now=datetime.datetime.now().strftime('%Y-%m-%d--%H-%M')))

with open('.gitlab-ci.yml', 'w') as outfile:
    yaml.dump(data, outfile, default_flow_style=False)
